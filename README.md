# DNN Structured Pruning

`yapt` is a tool for structured pruning that actually transforms (trims) the model and gives speedup.

This tool can be installed like a python package (needs `setuptools`): `pip install ./`

- This will automatically install pytorch and pytorch-lightning.
